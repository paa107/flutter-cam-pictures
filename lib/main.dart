import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

// Ensure that plugin services are initialized so that `availableCameras()`
// can be called before `runApp()`
WidgetsFlutterBinding.ensureInitialized();

// Obtain a list of the available cameras on the device.
final cameras = await availableCameras();

// Get a specific camera from the list of available cameras.
final firstCamera = cameras.first;


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // скрытие метки "debag"
      debugShowCheckedModeBanner: false,
      title: 'New Application',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // шапка приложения
      appBar: AppBar(
        title: Text('AppBar Title'),
      ),
      body: SafeArea(
        left: true,
        top: true,
        right: true,
        bottom: true,
        minimum: EdgeInsets.all(16.0),
        child: Text("Some text..."),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Камера',
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}
